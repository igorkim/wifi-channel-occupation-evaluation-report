\documentclass[../ictm16_ch_occupation_final_report.tex]{subfiles}
\begin{document}	
	
\onlyinsubfile{\graphicspath{ {../images/} }}		
	
\section{Data evaluation}
This section describes methods available in \textit{FSVRAnalysis} class and provides examples for each of them. Later in the section \ref{results} we discuss more on the results we got using methods described in this section.
\subsection{Restoring a single data frame}
Following example plots the 10th data frame of \textit{filename1.DAT} dump. The input parameter of \textit{set\_data\_points} assumes that 1 is the last data frame in time (0 data frame), then 10 is a -9 frame on time line.
\FloatBarrier
\begin{lstlisting}[language=Python, caption=Custom reader interface call]
from FSVRAnalysis import FSVRAnalysis
from FSVRReader import FSVRReader
reader = FSVRReader("dat/filename1.DAT")
analyzer = FSVRAnalysis(reader)
analyzer.set_data_points(10)
analyzer.plot_frame() \end{lstlisting}
\begin{figure}[!h]
	\centering
	\includegraphics[width=0.7\textwidth]{single_frame}
	\caption{Example of single frame restoration}
	\label{fig:single_frame}
\end{figure}
\FloatBarrier

\subsection{Averaging over n data frames}
\FloatBarrier
\begin{lstlisting}[language=Python, caption=Custom reader interface call]
from FSVRAnalysis import FSVRAnalysis
from FSVRReader import FSVRReader
reader = FSVRReader("dat/BEACON-200-10-1.DAT")
analyzer = FSVRAnalysis(reader)
analyzer.set_data_points(1000)
analyzer.plot_avg_values() \end{lstlisting}
\paragraph{Note} Line 5 here (set the number of data frames) is optional, without that maximum number of points to be considered. \par
The \textit{plot\_avg\_values} takes the timestamp of first and the last date frame in order to calculate time axis (duration). \par 
Each point on figure \ref{fig:avg_plot} represents average value over the all frequencies of one single data frame. A single data frame as on the figure \ref{fig:single_frame}. \par
Depending on the chosen sweep time, there are could be several points very close to each other. \par
\begin{figure}[!h]
	\centering
	\includegraphics[width=0.7\textwidth]{avg_plot}
	\caption{Example of average values over time}
	\label{fig:avg_plot}
\end{figure}
\FloatBarrier

\subsection{Cumulative distribution function (CDF) over n data frames}
For better analysis of data Cumulative distribution function is implemented. 
\FloatBarrier
\begin{lstlisting}[language=Python, caption=Custom reader interface call]
from FSVRAnalysis import FSVRAnalysis
from FSVRReader import FSVRReader
reader = FSVRReader("dat/BEACON-200-10-1.DAT")
analyzer = FSVRAnalysis(reader)
analyzer.set_data_points(1000)
analyzer.plot_cdf() \end{lstlisting}
\paragraph{Note} Line 5 here (set the number of data frames) is optional, without that maximum number of points to be considered. \par
Time difference between each two points is calculated. Then all values are sorted and CDF is calculated. \par
From the figure \ref{fig:cdf_plot} it is clear that the most frequent time difference is 100 ms, which is default beacon time for the measured wi-fi hotspot.
\begin{figure}[!h]
	\centering
	\includegraphics[width=0.7\textwidth]{cdf_plot}
	\caption{Example of CDF graph}
	\label{fig:cdf_plot}
\end{figure}
\FloatBarrier

\subsection{Markov's chain transitions for thresholds}
In order to calculate Markov's chain transitions, several thresholds must be defined as on listing \ref{lst:thresholds}. \par
\begin{figure}[!h]
	\centering
	\includegraphics[width=0.7\textwidth]{thresholds}
	\caption{Example of several reference levels}
	\label{fig:thresholds}
\end{figure}
\FloatBarrier
Listing \ref{lst:markovs} corresponds to figure \ref{fig:thresholds}. Result array \textit{result} contains number of transitions from state to state. Columns and rows corresponds to transitions between states. State transition for listing \ref{lst:markovs} explained with figure \ref{fig:states} and table \ref{tbl:markovs}.\par
Resulting array is always square and the main diagonal refers to the case when there is no transition (self-loop).\par
\FloatBarrier
\begin{lstlisting}[language=Python, caption=Standard deviation sample calculation, label=lst:thresholds]
from FSVRAnalysis import FSVRAnalysis
from FSVRReader import FSVRReader

reader = FSVRReader("dat/BEACON-200-10-1.DAT")
analyzer = FSVRAnalysis(reader)
analyzer.set_thresholds([-90, -70, -50])\end{lstlisting}
\FloatBarrier
Be aware that by default transitions from state 0 and to state 0 are not considered. In order to do that, the input parameter should be set to \textit{True} as \textit{generate\_markovs\_transitions(True) }. \par
\begin{table}[htbp]
	\label{tbl:markovs}
	\caption{Markov's states transitions}
	\begin{tabularx}{\textwidth}{| X | X | X | X |}
		\hline
		0 to 0 & 0 to 1 & 0 to 2 & 0 to 3 \\ \hline
		1 to 0 & 1 to 1 & 1 to 2 & 1 to 3 \\ \hline
		2 to 0 & 2 to 1 & 2 to 2 & 2 to 3 \\ \hline
		3 to 0 & 3 to 1 & 3 to 2 & 3 to 3 \\ \hline
	\end{tabularx}
\end{table}
\FloatBarrier

\begin{figure}[!h]
	\centering
	\includegraphics[width=0.7\textwidth]{states}
	\caption{States of Markov's chain}
	\label{fig:states}
\end{figure}
\FloatBarrier

\begin{lstlisting}[language=Python, caption=Markov chain sample calculation, label=lst:markovs]
from FSVRAnalysis import FSVRAnalysis
from FSVRReader import FSVRReader

reader = FSVRReader("dat/BEACON-200-10-1.DAT")
analyzer = FSVRAnalysis(reader)
analyzer.set_thresholds([-90, -70, -50])
result = analyzer.generate_markovs_transitions() 
'''
	array([	[ 0,  0,  0,  0],
					[ 0,  0,  1,  0],
					[ 0,  0,  2,  8],
					[ 0,  1,  6, 52]])
'''
result0 = analyzer.generate_markovs_transitions(True) 
'''
array([	[4917,    0,    4,    7],
				[   1,    0,    0,    0],
				[   4,    0,    1,    5],
				[   6,    1,    5,   48]])
'''
\end{lstlisting}

\subsection{Standard deviation}
There is a possibility to calculate standard deviation for several dump files. However standard deviation analysis was not a part of our project, that our Python tool was used for other project groups and this method has been implemented. The resulting array contains average levels of each collection of data points and the second array consists of the standard deviation from the average. \par
\FloatBarrier
\begin{lstlisting}[language=Python, caption=Standard deviation sample calculation, label=lst:std_dev]
avgs = []
devs = []
folder = "dat/"
files = ["filename1.DAT", "filename2.DAT", "ap_empty_002.DAT", "BEACON-200.DAT"]
reader = FSVRReader()
analyzer = FSVRAnalysis(reader)
analyzer.set_threshold(-90)
avgs, devs = analyzer.prepare_avg_std_dev(files, folder)
analyzer.plot_avg_std_dev(avgs, devs) \end{lstlisting}
\FloatBarrier
\begin{figure}[!h]
	\centering
	\includegraphics[width=0.7\textwidth]{std_dev}
	\caption{Example of standard deviation graph}
	\label{fig:std_dev}
\end{figure}
\FloatBarrier

\end{document}